package Pk01;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Ex3Diccionaris {
	
	public static void main(String[] args){
		
	Map<String, Integer> map = new HashMap <String, Integer>();
	
	/*Ingredients*/
	map.put("tomaquets", 6);
	map.put("flams" , 4);
	map.put("pizzes" , 2);
	map.put("llaunes de tonyina" , 2);
	map.put("blat de moro" , 5);
	map.put("enciam" , 1);
	
	Set<String> ingredients = map.keySet();
	
	
	
	System.out.println("Ingredients sense quantitat:\n ");
	for (String ingredient : ingredients){
	    System.out.println(ingredient);
	}
	
	System.out.println("\n");
	System.out.println("Quantitat de tomaquets:\n ");
	for (String ingredient : ingredients){
		
		if(ingredient.equals("Tomaquets")){
			System.out.println(map.get(ingredient));
		}
	}
	
	System.out.println("Ingredients amb mes de 3 unitats:\n ");
	for (String ingredient : ingredients){
		
		if(map.get(ingredient)>=3){
			System.out.println(ingredient);
		}
	}
	
	Scanner sc = new Scanner( System.in );
	
	
	String item;
	boolean itemTrobat=false;
	
	/*Compra de ingredients*/
	System.out.println("Quin ingriedent vols comprar?");
	item = sc.nextLine();
	
	for (String ingredient : ingredients){
		if(ingredient.equals(item)){
			System.out.println("Doncs tindras que comprar comprar: "+map.get(ingredient));
			itemTrobat = true;
		}
	}
	if (!itemTrobat){
			System.out.println("Ingredient incorrecte.");
		}
	

	System.out.println("Tots els ingredients amb quantitat:\n ");
	
	for (String ingredient : ingredients){
		
		System.out.println(ingredient+"( "+map.get(ingredient)+")");
		
	}
	
	int sumaIngredients=0;
	
	System.out.println("Suma de ingredients:\n ");
	
	for (String ingredient : ingredients){
		
		sumaIngredients+=map.get(ingredient);
		
	}
	
	System.out.println("Es tindran que comprar: "+sumaIngredients+ "ingredients");
	
	
	String ingredientModificable;
	int novaQuantitat;
	
	System.out.println("Quin ingredient vols modificar? ");
	ingredientModificable = sc.nextLine();
	System.out.println("Introdueix una nova quantitat pel ingredient: ");
	novaQuantitat=sc.nextInt();
	
	for (String ingredient : ingredients){
		
		if(ingredient.equals(ingredientModificable) ){
			
			map.put(ingredientModificable, novaQuantitat);
			
			
		}
		
	}
	
	
	
	
	
	}
	

}
