package Pk1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class Ex5Diccionaris {
	
	
	public static void main(String[] args){
	
	Map<String, String> paisos = new HashMap <String, String>();
	
	
	paisos.put("Espanya", "Madrid");
	paisos.put("Fran�a", "Par�s");
	paisos.put("It�lia", "Roma");
	paisos.put("Anglaterra", "Londres");
	paisos.put("Alemanya", "Berl�n");
	
	
	Scanner sc = new Scanner(System.in);
	String pais;
	String capital;
	String siNo;
	Set<String> claus = paisos.keySet();
	
	do{
	
	System.out.println("Introdueix el nom del pais (Primera lletra amb majuscula) existent o que vulguis introduir: ");
	pais = sc.nextLine();
	
	if(paisos.containsKey(pais)){
		System.out.println("La capital de "+ pais +" es: "+paisos.get(pais));
	}
	
	else if (!paisos.containsKey(pais)){
		
		System.out.println("Es posible que no hagis posat la primera lletra majuscula? ");
		siNo = sc.nextLine();
		
		if (siNo.equals("si") || siNo.equals("Si")){
			
		}
		
		else if (siNo.equals("no") || siNo.equals("No")){
			
			System.out.println("Quina es la capital del nou pais? ");
			capital=sc.nextLine();
			
			paisos.put(pais, capital);
		}
		
		
		
		
	}
	
		
		
		
	
	}while (!pais.isEmpty());
	

	
	}
	

}
