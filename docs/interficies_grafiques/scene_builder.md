## Scene builder

Les interfícies gràfiques en JavaFX es poden crear mitjançant codi Java, o
mitjançant un fitxer XML.

L'Scene Builder és un programa que proporciona Oracle i que permet generar
els fitxers XML d'una interfície gràfica de forma visual.

Per tal de treballar amb JavaFX necessitarem configurar l'entorn de treball.

### Configuració de l'Eclipse

#### Debian GNU/Linux

A la versió Jessie, el Java 8 es troba disponible al dipòsit de backports:

- Afegir:

```
deb http://http.debian.net/debian jessie-backports main
```

a */etc/apt/sources.list*.

- Instal·lar els paquets *openjdk-8-jdk*, *openjdk-8-doc* i *openjfx*.

Per tal que l'Eclipse utilitzi per defecte Java 8:

- Baixar Eclipse IDE for Java EE Developers

- Afegir a l'eclipse.ini les línies:

```
-vm
/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
```

- A l'Eclipse, instal·lar *e(fx)clipse install*

Per tal de tenir la documentació de Java i de JavaFX en local:

- Instal·lar els paquets *openjdk-8-doc* i *libopenjfx-java-doc*.

Per indicar a l'Eclipse on és la documentació de JavaFX:

- Anar a *Window->Preferences->Java->Installed JREs->java-8-openjdk-amd64->Edit...*

- Desplegar *...jfxrt.jar->Javadoc Location...*

- Posar: *file:/usr/share/doc/libopenjfx-java/api/*

#### Windows

Assegurar-se que l'Eclipse utilitza Java 8 per defecte:

- Anar a *Window->Preferences->Java->Installed JREs*

Si no està utilitzant Java 8:

- Afegir *Standard VM* i seleccionar el directori d'instal·lació del JDK 8.

- Assegurar-se que és el que utilitza per defecte.

- A *Java->Compiler* assegurar-se que utilitza *Compiler compliance level* 1.8

- A l'Eclipse, instal·lar *e(fx)clipse install*

***TODO: instal·lació de la documentació de Java i JavaFX en local a Windows***

### Instal·lació Scene Builder

Oracle només proporciona el codi font de l'Scene Builder. Per evitar
compilar-nos el programa nosaltres mateixos, el podem baixar de
[Gluon Scene Builder](http://gluonhq.com/open-source/scene-builder/).

A l'Eclipse li hem d'indicar on tenim l'executable de l'Scene Builder. Això es
fa a *Windows->Preferences*, anem a la secció *JavaFX* i a *SceneBuilder
Executable* indiquem la ruta a l'executable de l'Scene Builder.
