## Transaccions

Sovint ens trobem en situacions en què hem de fer diverses modificacions a
les dades d'una BD i garantir que totes aquestes modificcions es duen a
terme, o ve que cap ho fa.

Per exemple, en un traspàs de diners d'un compte bancari a un altre s'han
de fer dues operacions: retirar uns diners del compte origen i ingressar-los
al compte de destí. No hauríem de permetre que per una errada en algun punt
(el servidor de BD, la xarxa, el nostre programa...), és fes una de les
operacions però l'altra no.

Les **transaccions** permeten agrupar diverses operacions contra una base de
dades i tractar-les com si es tractés d'una sola operació. En cas que el
procés falli, tenim l'opció de desfer aquelles operacions que ja s'hagin
enviat. Un cop realitzades totes les operacions de la transacció correctament
podem confirmar-la, de manera que tots els canvis passen definitivament a la
base de dades.

### *Autocommit*

Si no diem el contrari, quan treballem amb JDBC contra MySQL tenim la
funcionalitat d'*autocommit* activada. Això significa que cadascuna de les
operacions que realitzem contra la base de dades es considera una
transacció que és confirmada automàticament quan s'ha executat amb èxit.

L'avantatge d'això és que ens podem oblidar de les transaccions i sabem que
qualsevol cosa que poguem expressar com una única sentència SQL s'executarà
sencera o fallarà sense fer canvis a la base de dades.

El problema però ve quan necessitem que diverses sentències s'executin com
una única transacció. Aleshores hem de deshabilitar l'*autocommit* i agafar
nosaltres el control per indicar quan comença i acaba cadascuna de les
transaccions.

Amb l'*autocommit* desactivat, la totalitat de consultes executades no són
definitives fins que es crida el mètode *commit()* explícitament.

En canvi, si volem anul·lar tots els canvis que s'han fet dins de la
transacció, ho podem fer amb el mètode *rollback()*.

### Exemple

Al següent exemple apujarem el sou a un empleat. L'empleat en qüestió és en
Georgi Facello i volem apujar-li el sou un 5% sobre el seu sou actual. Cal
vigilar perquè hi ha dos empleats amb el mateix nom. En el nostre cas volem
augmentar el sou de l'empleat contractat el 26 de juny de 1986.

Modificar un sou implica dues operacions sobre la taula *salaries*: posar
om a data de finalització del sou actual el dia d'avui, i afegir-hi una fila
nova fent constar el nou sou, la data d'inici i, com a data final, posar
el valor especial 9999-01-01.

Per garantir la coherència de les dades hem d'assegurar que aquestes dues
operacions es fan dins de la mateixa transacció:

```java
public class ExempleTransaccio {

	public static void main(String[] args) {
		int empNo = 0;
		int currentSalary = 0;

		// Sentències SQL que necessitarem
		String findEmpNoSQL = "select emp_no from employees " +
					"where first_name=? and last_name=? and hire_date=?";
		String findCurrentSalarySQL = "select salary from salaries " +
					"where emp_no=? and to_date='9999-01-01'";
		String closeSalarySQL = "update salaries set to_date=curdate() " +
					"where emp_no=? and to_date='9999-01-01'";
		String openSalarySQL = "insert into salaries " +
					"values (?, ?, curdate(), '9999-01-01')";

		// Establim connexió
		try (Connection con = DriverManager.getConnection(
				"jdbc:mariadb://localhost:3306/employees", "root", "usbw")) {
			// Creem els PreparedStatement
			try (PreparedStatement findEmpNoSt = con.prepareStatement(findEmpNoSQL);
					PreparedStatement findCurrentSalarySt = con.prepareStatement(findCurrentSalarySQL);
					PreparedStatement closeSalarySt = con.prepareStatement(closeSalarySQL);
					PreparedStatement openSalarySt = con.prepareStatement(openSalarySQL);) {
				// Desactivem autocommit per iniciar transacció
				con.setAutoCommit(false);

				// Cerquem el número de l'empleat
				findEmpNoSt.setString(1, "Georgi");
				findEmpNoSt.setString(2, "Facello");
				findEmpNoSt.setString(3, "1986-06-26");
				try (ResultSet rs = findEmpNoSt.executeQuery();) {
					if (rs.next()) {
						empNo = rs.getInt(1);
						if (rs.next()) {
							throw new SQLException("Hi ha més d'un treballador amb aquestes dades.");
						}
					} else {
						throw new SQLException("No hi ha cap treballador amb aquestes dades.");
					}
				}

				// Cerquem el sou actual de l'empleat
				findCurrentSalarySt.setInt(1, empNo);
				try (ResultSet rs = findCurrentSalarySt.executeQuery();) {
					rs.next();
					currentSalary = rs.getInt(1);
				}

				// Finalitzem el sou actual
				closeSalarySt.setInt(1, empNo);
				closeSalarySt.executeUpdate();
				// Assignem el nou sou
				openSalarySt.setInt(1, empNo);
				openSalarySt.setInt(2, (int) Math.round(currentSalary * 1.05));
				openSalarySt.executeUpdate();

				// Confirmem els canvis per finalitzar la transacció
				con.commit();
				System.out.println("Transacció realitzada!");
			} catch (SQLException e) {
				// En cas d'error, tornem la base de dades al seu estat inicial
				System.err.print("S'ha produït una errada a la transacció, desfem els canvis");
				con.rollback();
			} finally {
				// Finalment reactivem l'autocommit
				con.setAutoCommit(true);
			}
		} catch (SQLException e) {
			System.err.println("Error establint connexió: " + e.getMessage());
		}
	}
}
```

Els passos a seguir per fer una transacció sempre són els mateixos:

1. Posar *autocommit* a *false*.
2. Realitzar totes les actualitzacions de dades.
3. Si no hi ha hagut error i els resultats han estat els esperats, confirmar
els canvis amb *commit*.
4. Si hi ha hagut algun error, desfer els canvis amb *rollback*.
5. Tornar a posar l'*autocommit* a *true* per a no afectar a la resta del
programa.
