### Variables, mètodes i classes final

#### Variables final

Una variable declarada com a final només es pot inicialitzar una vegada.
Això no significa que s'hagi de fer necessàriament en la declaració de
la variable, sinó que es pot fer més endavant, quan ja hi hagi prou
informació per saber quin valor ha de tenir.

#### Mètodes final

Un mètode es declara **final** quan no volem que es pugui sobreescriure a
una classe derivada. Això s'utilitza per prevenir errors quan una
subclasse modifica un mètode que és crucial per al correcte funcionament
intern de la classe. En cas de dubte, és preferible no declarar com a
final un mètode, ja que és complicat preveure totes les formes
d'estendre la nostra classe i posar mètodes final pot limitar aquesta
flexibilitat.

Hi ha pocs exemples de mètodes final a les API de Java. Un d'ells
l'acabem de veure: el mètode *getClass()* d'*Object*. El resultat de
sobreescriure aquest mètode per retornar un tipus diferent d'objecte
*Class* que el que li correspon a l'objecte seria desastrós.

#### Classes final

Una classe es declara **final** quan no es vol que es puguin crear classes
derivades d'aquesta classe. Per exemple, la classe *String* és final.
Això s'ha fet així perquè la classe *String* és immutable, i no es vol
que una classe derivada de *String* ho pugui canviar.

En el cas de *String*, si volem una cadena que es pugui modificar haurem
d'utilitzar per exemple *StringBuilder*. El motiu pel qual s'ha
dissenyat *String* de forma que sigui immutable té a veure amb els
problemes que podem tenir amb els objectes mutables que emmagatzem com
atributs en una classe, i pels quals es manté una referència externa.

Ho podem veure en el següent exemple:

```java
public class Classe {
   private Rectangle r;
   private int area;

   public Classe(Rectangle r) {
       setRectangle(r);
   }

   @Override
   public String toString() {
       return r.toString();
   }

   public int getArea() {
       return area;
   }

   public void setRectangle(Rectangle r) {
       this.r = r;

       area = r.width * r.height;
   }
}
```

En aquest exemple, estem calculant l'àrea del rectangle (*Rectangle* és
una classe de les API de Java) en el mateix moment que el rebem, de
manera que ens estalviem de calcular-la cada vegada que ens criden
*getArea()*. Com que *r* és una variable privada que només es pot
modificar a través de *setRectangle*, podem estar segurs que mai no hi
haurà una incongruència entre el rectangle que tenim i l'àrea calculada.
O no?

Imaginem el següent programa:

```java
public class Programa {
   public static void main(String[] args) {
       Rectangle rect = new Rectangle(2, 3, 4, 5); // x, y, amplada, alçada
       Classe cl = new Classe(rect);
       rect.width = 1000;

       System.out.println(cl);
       System.out.println("Àrea: "+cl.getArea());
   }
}
```

El resultat d'executar aquest codi és que ens mostrarà que el nostre
rectangle de 1000x5 té una àrea de 20! El problema ha estat el fet de
mantenir una referència al rectangle intern de *cl*, que ens ha permet
modificar un atribut privat.

Si *Rectangle* hagués estat immutable això no hagués pogut passar mai,
perquè no s'hagués pogut executar la sentència *rect.width = 1000;*

Com que *String* s'usa de forma tant intensiva, les probabilitats de
caure en aquest tipus d'errors augmenten, i per això el fer-la immutable
i final és una bona mesura de seguretat.

I com arreglem el problema que tenim a l'exemple? Doncs fent una còpia
profunda del rectangle en comptes d'assignar una referència:

```java
    public void setRectangle(Rectangle r) {
       this.r = new Rectangle(r); // o this.r = r.clone();

       area = r.width * r.height;
   }
```
