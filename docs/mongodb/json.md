## El format JSON

Per treballar amb MongoDB és molt important entendre el format de documents
JSON.

L'acrònim JSON ve de *JavaScript Object Notation* i, de fet, els objectes
en JavaScript es poden exportar i importar en JSON directament. Però el JSON,
igual que l'XML, s'ha convertit en un format estàndard d'intercanvi de dades
entre aplicacions. De fet, és molt similar a l'XML, encara que més compacte.

La documentació oficial de JSON es pot consultar [aquí](http://www.json.org/).

Un document, o un objecte, que en aquest context són sinònims, és simplement
un conjunt de parelles *camp/valor*. El document es delimita pels símbols {},
i el símbol : separa un camp del seu valor. La , s'utilitza per separar les
parelles. Els camps i, si són text, els valors, es delimiten amb ".

**Exemple:**

```json
{
  "camp1": "valor1",
  "camp2": "valor2",
  "camp3": 3,
  "camp4": 4.5,
  "camp5": true,
  "camp6": false,
  "camp7": null
}
```

Els valors poden ser també arrays:

```json
{
  "camp1": ["valor1", "valor2", 3, 4.5]
}
```

I un valor també pot ser un altre document:

```json
{
  "camp1": {
    "camp11": "valor1",
    "camp12": "valor2"
  },
  "camp2": [
    "valor21", {
      "camp22": "valor22",
      "camp23": "valor23"
    }
  ]
}
```

Amb això ja podem crear estructures complexes, que incloguin documents dins
d'altres documents o dins d'arrays, en diverses capes. Tot i l'aparent
complexitat que pot tenir un document, no hem de perdre de vista que tots es
creen seguint aquesta sintaxi tan senzilla que acabem de veure.
