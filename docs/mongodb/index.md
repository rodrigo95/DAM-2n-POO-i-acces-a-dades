## Ús dels índexos

Un **índex** és un sistema que permet accelerar algunes cerques sobre una base
de dades.

La idea és evitar haver de fer una cerca linial cercant els valors que
ens interessen. En comptes d'això, en un pas previ, es recorren tots els valors
dels camps que ens interessen i s'emmagatzema en quina o quines posicions
apareixen cadascun dels valors que hi ha.

L'ús d'índexs té l'avantatge evident d'accelerar algunes consultes, però a
canvi de gastar més espai d'emmagatzematge. A més, els índexs s'han
d'actualitzar cada vegada que un document s'afegeix o es modifica (si s'ha
modificat algun dels camps indexats). Això fa que aquestes operacions siguin
una mica més lentes.

En una base de dades relacional es creen índexs automàticament per a totes
les claus primàries, claus forànies i claus úniques. Això fa que, encara que
es puguin crear altres índexs, no sigui molt habitual.

En canvi, en MongoDB només es crea un índex automàtic pels identificadors
de cada document. Una de les decisions de disseny més importants que ens
trobem quan creem una base de dades en MongoDB és decidir quins índexs
necessitem per tal d'optimitzar les consultes més habituals o més crítiques
pels nostres casos d'ús concrets.

### Creació i test d'índexos des de Java

Per poder provar els índexs, primer de tot crearem dues col·leccions amb molts
documents:

```java
MongoClient client = new MongoClient();
MongoDatabase db = client.getDatabase("exemples");
MongoCollection<Document> coll = db.getCollection("punts");
MongoCollection<Document> coll2 = db.getCollection("punts2");

coll.drop();
coll2.drop();

for (int x=0; x<1000; x++) {
	for (int y=0; y<1000; y++) {
		coll.insertOne(new Document("x",x).append("y", y));
		coll2.insertOne(new Document("x",x).append("y", y));
	}
	if (x%10==0)
		System.out.print(".");
}
System.out.println();
```

Aquesta operació tarda una estona i crea un total d'un milió de documents a
cada col·lecció.

Com que el MongoDB utilitza caus per accelerar les consultes que ja s'han fet
recentment, hem de vigilar molt com fem les proves de velocitat. Si ja hem fet
el test abans i el volem repetir (sense recrear les col·leccions), podem
executar, des de la línia de comandes del MongoDB, les següents
instruccions:

```
db.punts.getPlanCache().clear()
db.punts2.getPlanCache().clear()
```

Amb això buidem el cau de consultes i ens assegurem que s'hagin de cercar de
nou els documents.

Crearem un índex només a una de les dues col·leccions. El creem de manera
que ens acceleri les cerques i ordenacions pel camp *y*:

```java
coll.createIndex(new Document("y",1), (new IndexOptions()).name("index_y"));
```

L'1 que hem posat al document que hem utilitzar per crear l'índex indica que
es guardaran les posicions de tots els valors de *y* ordenats de forma
ascendent. Un -1 indicaria descendent.

Farem una consulta que ens retorni un document en les dues
col·leccions. Utilitzem `System.currentTimeMillis()` abans i després
de l'execució per calcular el temps que tarda.

```java
long startTime = System.currentTimeMillis();
coll.find(Filters.eq("y", 500)).forEach((Document doc)->{});
long endTime = System.currentTimeMillis();
System.out.println("Execució amb índex: "+(endTime-startTime));
```

```java
long startTime = System.currentTimeMillis();
coll2.find(Filters.eq("y", 500)).forEach((Document doc)->{});
long endTime = System.currentTimeMillis();
System.out.println("Execució sense índex: "+(endTime-startTime));
```

Per millorar els resultats hem creat primer les col·leccions i posteriorment
hem executat la consulta sobre una de les col·leccions en una execució, i en
una altra execució ho hem fet sobre l'altra col·lecció.

Els resultats són que la consulta amb índex ha tardat uns 550 mil·lisegons,
mentre que sense índex ha tardat uns 1200 mil·lisegons. Això ens diu que
l'execució del programa és aproximadament el doble de ràpida si utilitzem
l'índex que si no ho fem.

[Codi complet de l'exemple](codi/mongodb/src/main/java/mongodb/ExempleIndexos.java)

### Creació i test d'índexos des de mongo

Per a crear el mateix índex que abans hem creat en Java des de la línia de
comandes del MongoDB ho hauríem pogut fer amb la sentència:

```
db.punts.createIndex({y:1})
```

MongoDB té la utilitat *explain* que ens permet analitzar la forma com
s'executarà una consulta.

Per exemple, si ho provem sobre la col·lecció on hem creat l'índex amb:

```
db.punts.find({y:500}).explain()
```

Obtenim:

```
{
	"queryPlanner" : {
		"plannerVersion" : 1,
		"namespace" : "exemples.punts",
		"indexFilterSet" : false,
		"parsedQuery" : {
			"y" : {
				"$eq" : 500
			}
		},
		"winningPlan" : {
			"stage" : "FETCH",
			"inputStage" : {
				"stage" : "IXSCAN",
				"keyPattern" : {
					"y" : 1
				},
				"indexName" : "index_y",
				"isMultiKey" : false,
				"isUnique" : false,
				"isSparse" : false,
				"isPartial" : false,
				"indexVersion" : 1,
				"direction" : "forward",
				"indexBounds" : {
					"y" : [
						"[500.0, 500.0]"
					]
				}
			}
		},
		"rejectedPlans" : [ ]
	},
	"serverInfo" : {
		"host" : "spore",
		"port" : 27017,
		"version" : "3.2.1",
		"gitVersion" : "a14d55980c2cdc565d4704a7e3ad37e4e535c1b2"
	},
	"ok" : 1
}
```

Aquí hi ha molta informació, però en podem destacar alguna d'interessant.

Podem veure que hi ha un *winningPlan*, que és la forma com es decidiria
realitzar la consulta.

Dins del *winningPlan* hi ha diversos *stages*. Cada *stage* és una fase de
la resolució de la consulta. En el nostre cas, com que no realitzem cap
ordenació o projecció, només tenim dos *stage*: *FETCH* i *IXSCAN*.

*IXSCAN* fa referència a l'escaneig d'un índex. Podem veure quin índex
s'utilitzaria (*"indexName" : "index_y"*), en quin sentit
(*"direction" : "forward"*) i quin tram de l'índex ha de consultar
(*"indexBounds" : {"y" : ["[500.0, 500.0]"]}*).

La fase *FETCH* fa referència a agafar les dades dels registres que s'han
seleccionat a la cerca anterior.

Comparem ara això amb l'explicació que obtenim de la mateixa consulta, però
sobre la col·lecció on no tenim aquest índex:

```
> db.punts2.find({y:500}).explain()
{
	"queryPlanner" : {
		"plannerVersion" : 1,
		"namespace" : "exemples.punts2",
		"indexFilterSet" : false,
		"parsedQuery" : {
			"y" : {
				"$eq" : 500
			}
		},
		"winningPlan" : {
			"stage" : "COLLSCAN",
			"filter" : {
				"y" : {
					"$eq" : 500
				}
			},
			"direction" : "forward"
		},
		"rejectedPlans" : [ ]
	},
	"serverInfo" : {
		"host" : "spore",
		"port" : 27017,
		"version" : "3.2.1",
		"gitVersion" : "a14d55980c2cdc565d4704a7e3ad37e4e535c1b2"
	},
	"ok" : 1
}
```

Podem veure que ara apareix un únic *stage*, *COLLSCAN*. Aquest *stage* indica
que la forma com es resol la consulta és recorrent tota la col·lecció,
cercant els documents que compleixen les condicions del filtre.

Com de menys òptim és això?

*explain* té un paràmetre opcional que ens permet augmentar la quantitat
d'informació obtinguda. Provem de veure quins són els resultats concrets
d'executar la conslta en un i altre cas:

```
> db.punts.find({y:500}).explain("executionStats")
{
...
	"executionStats" : {
		"executionSuccess" : true,
		"nReturned" : 1000,
		"executionTimeMillis" : 9,
		"totalKeysExamined" : 1000,
		"totalDocsExamined" : 1000,
		"executionStages" : {
			"stage" : "FETCH",
			"nReturned" : 1000,
			"executionTimeMillisEstimate" : 10,
			"works" : 1001,
			"advanced" : 1000,
			"needTime" : 0,
			"needYield" : 0,
			"saveState" : 7,
			"restoreState" : 7,
			"isEOF" : 1,
			"invalidates" : 0,
			"docsExamined" : 1000,
			"alreadyHasObj" : 0,
			"inputStage" : {
				"stage" : "IXSCAN",
				"nReturned" : 1000,
				"executionTimeMillisEstimate" : 0,
				"works" : 1001,
				"advanced" : 1000,
				"needTime" : 0,
				"needYield" : 0,
				"saveState" : 7,
				"restoreState" : 7,
				"isEOF" : 1,
				"invalidates" : 0,
				"keyPattern" : {
					"y" : 1
				},
				"indexName" : "index_y",
				"isMultiKey" : false,
				"isUnique" : false,
				"isSparse" : false,
				"isPartial" : false,
				"indexVersion" : 1,
				"direction" : "forward",
				"indexBounds" : {
					"y" : [
						"[500.0, 500.0]"
					]
				},
				"keysExamined" : 1000,
				"dupsTested" : 0,
				"dupsDropped" : 0,
				"seenInvalidated" : 0
			}
		}
	},
...
}
```

A més de la informació anterior, ara ha aparegut un munt de dades noves.

Podem veure a *keysExamined* que s'han examinat 1000 claus de l'índex a la
fase *IXSCAN*. Això és molt òptim, perquè tenim justament 1000 documents que
compleixen el filtre de la nostra consulta.

A partir de les dades de l'índex, podem veure que ha hagut d'examinar un total
de 1000 documents (*"docsExamined" : 1000*), que són tots els que ens ha
retornat.

Comparem això amb l'execució de la consulta a l'altra col·lecció:

```
> db.punts2.find({y:500}).explain("executionStats")
{
...
	"executionStats" : {
		"executionSuccess" : true,
		"nReturned" : 1000,
		"executionTimeMillis" : 718,
		"totalKeysExamined" : 0,
		"totalDocsExamined" : 1000000,
		"executionStages" : {
			"stage" : "COLLSCAN",
			"filter" : {
				"y" : {
					"$eq" : 500
				}
			},
			"nReturned" : 1000,
			"executionTimeMillisEstimate" : 650,
			"works" : 1000002,
			"advanced" : 1000,
			"needTime" : 999001,
			"needYield" : 0,
			"saveState" : 7812,
			"restoreState" : 7812,
			"isEOF" : 1,
			"invalidates" : 0,
			"direction" : "forward",
			"docsExamined" : 1000000
		}
	},
...
}
```

Ara podem veure que s'han hagut d'examinar un milió de documents
(*"docsExamined" : 1000000*) que són tots els de la col·lecció!

També podem veure que el temps d'execució previst ha canviat molt. Abans
teníem un *executionTimeMillisEstimate* de 10 mil·lisegons, mentre que ara el
tenim de 650.

La diferència amb els temps obtinguts a les proves que hem abans amb Java es
deuen al recorregut dels documents, inicialització de la màquina virtual, i
altres aspectes que no tenen a veure amb MongoDB. Ara podem veure que la
consulta és 65 cops més ràpida si utilitzem un índex adequat.
