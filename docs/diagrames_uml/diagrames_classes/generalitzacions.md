## Generalitzacions

La relació de **generalització** és una relació entre dues classes que tenen
moltes similituds entre elles. Una d'elles, el supertipus, conté totes
les característiques comunes, mentre que l'altra, el subtipus, incorpora
algunes característiques pròpies.

El punt important aquí és que una instància del subtipus ha de ser també
una instància del supertipus.

Per exemple, la classe superior podria ser *PartCotxe*, i podria tenir una sèrie
de propietats, com el nom del fabricant, o el número de sèrie. Un *Neumàtic*
és una part d'un cotxe, i incorpora característiques noves, com la velocitat
que suporta i la durabilitat.

![Exemple generalització](docs/diagrames_uml/imatges/generalitzacio.png)

A nivell d'implementació les generalitzacions es tradueixen en herència. El
principi més important a l'hora de decidir si cal aplicar herència o no en
la relació entre dues classes és el de **substituibilitat**: he de poder
passar un Neumàtic a qualsevol operació que necessiti una PartCotxe, i tot
ha de funcionar correctament.

L'herència és un mecanisme potent, però de vegades té efectes indesitjables.
No hi ha cap forma d'evitar que una subclasse heredi tots els atributs i
mètodes de la classe de la qual deriva, i en ocasions això no és el que es
desitja. La implementació d'interfícies i molts dels patrons de disseny estan
pensats com a mecanismes alternatius a l'herència.

### Generalització contra classificació

Cal evitar la utilització de la generalització (herència) en casos en què la
relació no és *transitiva*. Entenem per relació transitiva el fet que si
la classe *C* deriva de la classe *B* i la classe *B* deriva de la classe
*A*, un objecte de tipus *C* és, a més de de tipus *B*, de tipus *A*.

Per exemple, un pastor alemany és un gos, i els gossos són animals. Així,
podem assegurar que un pastor alemeny és un animal.

![Generalització vàlida](docs/diagrames_uml/imatges/generalitzacio_versus_classificacio.png)

Considerem però el següent exemple: un pastor alemany és un gos, i els
gossos són una espècie. Per tant, un pastor alemany és una espècie.

![Generalització incorrecta](docs/diagrames_uml/imatges/generalitzacio_versus_classificacio_001.png)

Evidentment, l'última conclusió és errònia. La relació entre gos i espècie
no és de generalització, sinó de classificació. En aquest tipus de situacions
aquestes relacions no es modelen bé utilitzant herència. En canvi, es
podria utilitzar una simple dependència entre gos i espècie per indicar
aquest fet:

![Instanciació](docs/diagrames_uml/imatges/generalitzacio_versus_classificacio_002.png)