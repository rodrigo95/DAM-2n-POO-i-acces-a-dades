## Instal·lació del Laravel Framework

Hi ha dues formes d'instal·lar Laravel: en el propi sistema, instal·lant
prèviament tots els requeriments (servidor web, llenguatge PHP, etc.) amb les
extensions necessàries, o utilitzant una màquina virtual preparada.

El segon cas resulta còmode per una instal·lació en un entorn de
desenvolupament. Laravel ja proporciona una màquina virtual amb tot el
programari i configuracions necessàries.

Nosaltres hem d'instal·lar la màquina virtual i compartir algunes carpetes
entre el nostre sistema i la màquina virtual, de manera que poguem anar
desenvolupant la web que fem i que la màquina virtual vegi els canvis
automàticament.

La informació detallada d'aquest tipus d'instal·lació es pot trobar a
https://laravel.com/docs/5.2/homestead.

Nosaltres desenvoluparem la primera opció.

### Instal·lació dels prerequisits

Necessitarem instal·lar un servidor web, un servidor de bases de dades, i
el llenguatge PHP amb algunes extensions.

#### Debian

Instal·larem els següents paquets:

- Servidor web: *apache2*
- Extensió d'apache per usar PHP: *libapache2-mod-php5*
- Extensió de criptorgrafia pel PHP: *php5-mcrypt* (no és imprescindible en
  general, però sí si la nostra web necessita validació d'usuaris)
- Servidor de base de dades: *mariadb-server*
- Extensió del PHP per connectar-se a MySQL/MariaDB: *php5-mysql*

#### Windows

Podem utilitzar un paquet que inclogui totes les dependències, com el
[XAMPP](https://www.apachefriends.org/index.html).

### Instal·lació del Composer

El *Composer* és un programa PHP que facilita la instal·lació i la gestió de
dependències entre paquets PHP.

Es tracta d'un programa en línia de comandes inclòs en un sol fitxer que podem
descarregar de https://getcomposer.org/download/.

Creem un directori on desenvoluparem el nostre projecte, i copiem el fitxer
descarregat allà.

### Instal·lació de Laravel

Utilitzarem el Composer per baixar el Laravel i crear l'estructura de la nostra
web.

En una terminal, ens situem al directori on hem situat el Composer i executem:

```
php composer.phar global require "laravel/installer"
php composer.phar create-project --prefer-dist laravel/laravel <nom_projecte>
```

La primera instrucció descarrega l'instal·lador de Laravel i la segona ens
crea l'estructura de la nostra web i baixa totes les dependències del framework.

En Windows, el PHP pot no estar a la ruta del sistema. Per afegir-lo:

- Botó dret a l'ordinador.
- Seleccionem *Propietats*.
- Cliquem a *Configuració avançada del sistema*.
- Cliquem a *Variables d'entorn...*.
- Seleccionem *PATH* i cliquem a *Edita...*
- Al final del *PATH* afegim un punt i coma seguit de la ruta on hi ha
l'executable del PHP, per exemple `d:\xampp\php`.

Un cop fet això ja podem executar les dues línies anteriors.

### Configuració de la instal·lació

Hem d'assegurar que el servidor web tindrà accés d'escriptura a dos directoris
concrets de la instal·lació. En un entorn de desenvolupament, n'hi ha prou
amb deixar que tothom hi pugui accedir.

Els directoris són: *storage* i *bootstrap/cache*.

En Debian, ens situem a l'arrel del projecte i executem:
`chmod -R 777 storage bootstrap/cache`.

Si utilitzem Windows amb XAMPP aquest pas no fa falta, ja que el XAMPP
s'està executant amb el mateix usuari amb què desenvolupem i, per tant, té tots
els permisos sobre tots els seus fitxers.

### Configuració de l'Apache

Hem d'indicar-li a l'Apache on és l'arrel de la nostra web.

#### Debian

Editem `/etc/apache2/sites-available/000-default.conf` i posem:

```
DocumentRoot <ruta a l'arrel del nostre projecte>/public
```

On abans hi havia:

```
DocumentRoot /var/www
```

Editem `/etc/apache2/apache2.conf` i posem:

```
<Directory <ruta a l'arrel del nostre projecte>/public/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
</Directory>
```

On abans hi havia:

```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
</Directory>
```

Reiniciem el servidor: `systemctl restart apache2`

#### Windows

Al panell de control de XAMPP, anem a configurar l'apache, i seleccionem el
fitxer *httpd.conf*.

Les dues directives a modificar són les mateixes que en el cas de Debian.

Reiniciem el servidor mitjançant el panell de XAMPP.

### Comprovació

Utilitzem el navegador web i ens connectem a *localhost*. Hauríem de veure una
pàgina en blanc i un títol al mig amb el missatge *Laravel 5*.

### Configuració de l'aplicació

Finalment, hi ha alguns aspectes dels fitxers *.env* i *config/app.php* que ens
interessa revisar.

El fitxer *.env* conté paràmetres de configuració que són propis de cada
lloc on l'aplicació es pot desplegar, i de cada desenvolupador. Per tant,
aquest fitxer no s'ha d'incloure a un programa de control de versions.

En aquest fitxer podem configurar les dades d'accés a la base de dades de la
nostra aplicació.

A *config/app.php* hi ha paràmetres de configuració generals de l'aplicació.
Alguns d'ells (els que utilitzen la funció *env()*, s'estan agafant directament
del fitxer anterior).

A *app.php* podem configurar coses com el fus horari i l'idioma de l'aplicació.

Més informació sobre la configuració a
https://laravel.com/docs/5.2/configuration.
