## Migracions i llavors de base de dades

### Migracions

Les migracions ens permeten codificar l'estructura de la base de dades
directament utilitzant PHP. Això ens permet incorporar una gestió de versions
a la pròpia base de dades, de manera que el codi que utilitza una versió de
la base de dades estigui sempre en consonància amb l'estat de la base de dades.

A més, les migracions es poden fer o desfer, cosa que en facilita la gestió, i
conserven informació sobre quan s'han fet i quin canvi s'ha fet sobre
l'estructura de la base de dades cada vegada.

Per poder utilitzar les migracions primer cal crear una taula a la base de
dades que ens permeti saber quines migracions s'hi han aplicat i quines no.

Aquesta taula es pot crear utilitzant la comanda:

```
php artisan migrate:install
```

El programa *artisan* el podem trobar a l'arrel del nostre projecte i ens
ofereix una colla d'ordres útils per automatitzar molts processos comuns.

Un cop tenim la infraestructura necessària, podem començar a crear
migracions:

```
php artisan make:migration create_users_table --create=users
```

Això indica que volem crear una nova migració anomenada *create_users_table* i
que l'objectiu de la migració és crear una nova taula anomenada *users*.

Aquesta ordre es crearia un nou fitxer de nom la data en què hem executat
l'ordre, seguit de *create_users_table.php*, dins del directori
`database/migration`. En aquest fitxer hi haurà una plantilla que hem de
completar amb les operacions de la migració.

En aquest cas concret, no cal executar aquesta ordre, perquè la migració
per la taula d'usuaris ja ens ve preparada amb el següent codi:

```php5
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
```

El mètode *up()* és el que s'executa quan activem la migració i el mètode
*down()* s'executa si volem desfer-la.

La classe *Schema* té una colla de mètodes que permeten editar taules. Veiem
que la taula *users* tindrà un *id* autonumèric, i un nom i una contrasenya,
una adreça de correu electrònic que ha de ser única a la taula.

La columna *remember_token* es especifica per usuaris i s'utilitza per
protegir les sessions dels usuaris del segrest de cookies de sessió.

El mètode *timestamps()* crea dues columnes, *created_at* que contindrà el
moment de creació d'un registre i *updated_at* que guardarà el moment de
l'última modificació del registre. Aquestes dues columnes són útils per a
depurar i auditar la base de dades, i la seva inclusió és habitual en qualsevol
taula.

El mètode *down()* simplement elimina la taula *users*.

Imaginem ara que volem afegir algun camp als usuaris, per exemple, per guardar
el seu nom i cognoms.

Si no hem fet mai la migració i estem a l'inici del desenvolupament podem
editar directament aquest fitxer, però més habitualment,
voldrem afegir un cap nou i actualitzar el codi del programa associat a aquest
camp.

Altres desenvolupadors poden estar en un punt en què no vulguin
actualitzar la seva base de dades local, perquè encara no tenen el codi
adaptat. O ens pot interessar que quedi constància al sistema de control de
versions de quina és l'estructura de la base de dades que es correspon amb
cada estat del codi.

Així doncs, crearem una altra migració per fer aquests canvis:

```
php artisan make:migration add_firstname_lastname_to_users_table --table=users
```

I editarem el fitxer que s'ha creat:

```php5
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirstnameLastnameToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('firstname');
            $table->string('lastname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->removeColumn('firstname');
            $table->removeColumn('lastname');
        });
    }
}
```

Per aplicar les migracions utilitzem:

```
php artisan migrate
```

El sistema ja recorda (a la taula de migracions que hem creat abans) quines
migracions ja s'han aplicat i quines no, així que només aplicarà les
migracions noves.

Si volguéssim desfer l'última migració, ho podem fer amb:

```
php artisan migrate:rollback
```

També podem eliminar totes les migracions amb `php artisan migrate:reset`, o
desfer-les totes i tornar-les a aplicar, per tal de recrear la base de dades
sencera, amb `php artisan migrate:refresh`.

### Llavors

Les llavors s'utilitzen per poblar inicialment les bases de dades.

Habitualment, la major part de les dades d'una aplicació s'obtindran a través
d'una importació, o s'aniran introduïnt amb el temps a través de la pròpia
aplicació.

Però les llavors ens són útils per fer conjunts de dades de prova o per
inicialitzar alguns valors a la base de dades que sempre hi hagin de ser.

Un ús habitual, per exemple, és crear alguns usuaris inicials per provar
la nostra aplicació.

En aquest podem executar la comanda:

```
php artisan make:seeder UsersTableSeeder
```

Això ens generarà el fitxer *UsersTableSeeder.php* a `database/seeds`. Podem
completar el mètode *run()* d'aquesta classe per afegir les dades concretes
que volem inserir a la base de dades:

```php5
<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Mendax',
            'email' => 'mendax@wikileaks.org',
            'firstname' => 'Julian',
            'lastname' => 'Assange',
            'password' => bcrypt('ACollectionOfDiplomaticHistorySince_1966_ToThe_PresentDay#'),
        ]);
    }
}
```

La classe *DatabaseSeeder*, també a `database/seeds` és la classe on es van
a buscar els *seeders* per defecte.

Per tal que el nostre nou *seeder* s'executi, afegim la crida a aquesta classe:

```php5
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
    }
}
```

Finalment, quan volem executar els *seeders* que hem preparat, ho fem amb la
comanda:

```
php artisan db:seed
```

Si en qualsevol moment ens trobem que l'*artisan* no és capaç de localitzar
alguna de les classes que hem creat, hem de fer que es tornin a cercar.

Això ho podem fer, situats a l'arrel del projecte, amb la comanda:

```
php ../composer.phar dump-autoload
```
