package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class ParametresSQLEx4 {
	
	
		
		public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);
	        String client=" ";
	        String data1=" ";
	        String data2=" ";
	        String url = "jdbc:mysql://localhost:3306/Sakila";
	        Properties connectionProperties = new Properties();
	        connectionProperties.setProperty("user", "root");
	        try (Connection conn = DriverManager.getConnection(url, connectionProperties);
	                PreparedStatement st = (PreparedStatement) conn.prepareStatement( "SELECT py.payment_id, py.amount, fm.title "
	                        + "FROM (((payment py join customer ct on (py.customer_id=ct.customer_id)) "
	                        + "JOIN store str on (ct.store_id=str.store_id)) "
	                        + "JOIN inventory iv on (str.store_id=iv.store_id)) "
	                        + "JOIN film fm on (iv.film_id=fm.film_id) "
	                        + "WHERE ct.customer_id LIKE ?  and py.payment_date BETWEEN '?' 00:00:01.000 and '?' 23:59:59.000")) {
	        	
	            while (!client.equals("") && !data1.equals("") && !data2.equals("")) {
	                System.out.println("Introdueix el id del client: ");
	                client = sc.nextLine();
	                System.out.println("Introdueix la primera data (yyyy-mm-dd)");
	                data1 = sc.nextLine();
	                System.out.println("Introdueix la segona data (yyyy-mm-dd)");
	                data2 = sc.nextLine();
	                
	                if (!client.equals("") && !data1.equals("") && !data2.equals("")) {
	                	
	                	
	                	st.setString(1,client);
	                	st.setString(2,data1);
	                	st.setString(3,data2);
	                    try (ResultSet rs = st.executeQuery()) {
	                    		
	                           
	                    		
	                        while (rs.next()) {
	                            String paymentid = rs.getString("py.payment_id");
	                            String amount = rs.getString("py.amount");
	                            String title = rs.getString("fm.title");
	                           
	                            
	                            
	                            System.out.println("Payment: " +paymentid+  " Quantitat:  " +amount+ " Nom del item: " +title+ "\n" );
	                      
	                        }
	                    }
	                    
	                }
	                
	            }
	            
	        } catch (SQLException e) {
	            System.err.println("Error SQL: "+e.getMessage());
	        }
	        sc.close();
	    }

	}
