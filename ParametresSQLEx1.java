package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class ParametresSQLEx1 {
	
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String actors=" ";
        String url = "jdbc:mysql://localhost:3306/Sakila";
        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("user", "root");
        try (Connection conn = DriverManager.getConnection(url, connectionProperties);
                PreparedStatement st = (PreparedStatement) conn.prepareStatement("select fm.title "
                        + "from (actor ac join film_actor fa on (ac.actor_id=fa.actor_id)) JOIN film fm on (fa.film_id=fm.film_id) "
                        +  "WHERE last_name LIKE ?")) {
            while (!actors.equals("")) {
                System.out.println("Introdueix el nom del actor: ");
                actors = sc.nextLine();
                if (!actors.equals("")) {
                	
                	st.setString(1, actors);
                	
                    try (ResultSet rs = st.executeQuery( )) {
                    		
                        while (rs.next()) {
                            String movie = rs.getString("fm.title");
                            System.out.println("Pelicules de " +actors+  " : " +movie+ "\n" );
                      
                        } 
                    }
                    
                }
                
            }
            
        } catch (SQLException e) {
            System.err.println("Error SQL: "+e.getMessage());
        }
        sc.close();
    }

}
