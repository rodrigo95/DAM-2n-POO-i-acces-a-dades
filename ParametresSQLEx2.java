package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class ParametresSQLEx2  {
	
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String paraula_clau=" ";
        String url = "jdbc:mysql://localhost:3306/Sakila";
        Properties connectionProperties = new Properties();
        connectionProperties.setProperty("user", "root");
        try (Connection conn = DriverManager.getConnection(url, connectionProperties);
                PreparedStatement st = (PreparedStatement) conn.prepareStatement(""
                		+ "SELECT distinct f.title,st.store_id "
                		+ "FROM (film f join inventory i on (f.film_id=i.film_id)) "
                		+ "join store st on (i.store_id=st.store_id) "
                		+ "WHERE ( description LIKE ? or title LIKE ? )")) {
            while (!paraula_clau.equals("")) {
                System.out.println("Introdueix una paraula clau: ");
                paraula_clau = sc.nextLine();
               
                if (!paraula_clau.equals("") ) {
                	
                	st.setString(1,"%"+paraula_clau+"%");
                	st.setString(2,"%"+paraula_clau+"%");
                	
                    try (ResultSet rs = st.executeQuery()) {
                    		
                        while (rs.next()) {
                            
                            String title = rs.getString("f.title");
                            String store = rs.getString("st.store_id");
                            
                            
                            System.out.println("Pelicula: "+title+ " i Store: " + store + "\n" );
                      
                        }
                    }
                    
                }
                
            }
            
        } catch (SQLException e) {
            System.err.println("Error SQL: "+e.getMessage());
        }
        sc.close();
    }

}
