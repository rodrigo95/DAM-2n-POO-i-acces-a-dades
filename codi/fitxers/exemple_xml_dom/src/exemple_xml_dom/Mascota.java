package exemple_xml_dom;

import java.io.Serializable;

public class Mascota implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nom;
	private int numPotes;
	private boolean pel = true;
	
	public Mascota(String nom, int numPotes) {
		this.nom=nom;
		this.numPotes=numPotes;
	}
	
	public Mascota(String nom, int numPotes, boolean pel) {
		this(nom,numPotes);
		this.pel=pel;
	}

	public String getNom() {
		return nom;
	}

	public int getNumPotes() {
		return numPotes;
	}

	public boolean hasPel() {
		return pel;
	}
	
	@Override
	public String toString() {
		return "Mascota nom="+nom+" numPotes="+numPotes+" pel="+pel;
	}
	
}
