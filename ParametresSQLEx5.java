package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class ParametresSQLEx5 {
	
		public static void main(String[] args) {
			
			Scanner in = new Scanner(System.in);
			int botiga;
			int metode;
			String cerca;
			mostrarBotigues();
			System.out.println("\nIntrodueix el numero de la botiga: ");
			botiga = in.nextInt();
			System.out.println("\nCom vols cercar la pelicula?\n\n1.Pel nom\n2.Pel actor\n3.Per genere\n");
			metode = in.nextInt();
			
			switch(metode){
			
			case 1:
				System.out.println("Com es diu la pelicula?\n");
				cerca = in.next();
				cercaPeli(botiga, cerca);
				break;
			case 2:
				System.out.println("Quin actor surt a la pelicula?\n");
				cerca = in.nextLine();
				cercaActor(botiga, cerca);
				break;
			case 3:
				System.out.println("De quin genere es?\n");
				cerca = in.nextLine();
				cercaGenere(botiga, cerca);
				break;
			}
			
			in.close();
		}

		public static void mostrarBotigues(){
			String url = "jdbc:mysql://localhost:3306/sakila";
			Properties connectionProperties = new Properties();
	        connectionProperties.setProperty("user", "root");
	        try (Connection con = DriverManager.getConnection(url,connectionProperties);
	                Statement st = (Statement) con.createStatement();) {
	            try (ResultSet rs = st.executeQuery("SELECT s.store_id, a.address"
	            		+ " FROM store s JOIN address a ON s.address_id = a.address_id")) {
	            	System.out.println("Botigues disponibles: \n");
	                while (rs.next()) {
	                    String nom=rs.getString("s.store_id");
	                    String dir=rs.getString("a.address");
	                    System.out.println("Nom de botiga: "+nom+"\t Direccio: "+dir+"\t");
	                }
	            }
	        } catch (SQLException e) {
	            System.err.println("Error SQL: " + e.getMessage());
	        }
		}
		
		public static void cercaPeli(int botiga, String cerca){
			
			String url = "jdbc:mysql://localhost:3306/sakila";
			Properties connectionProperties = new Properties();
	        connectionProperties.setProperty("user", "root");
	        try (Connection con = DriverManager.getConnection(url,connectionProperties);
	            PreparedStatement st = (PreparedStatement) con.prepareStatement("SELECT f.title, r.return_date,"
	            		+ " DATE_ADD(r.rental_date, INTERVAL f.rental_duration DAY) AS retorn"
	            		+ " FROM film f JOIN inventory i ON f.film_id = i.film_id"
	            		+ " JOIN store s ON i.store_id = s.store_id"
	            		+ " JOIN rental r ON i.inventory_id = r.inventory_id"
	            		+ " WHERE s.store_id = ? AND upper(f.title) LIKE ?")) {          
	            		st.setInt(1, botiga);
	            		st.setString(2, "%"+cerca+"%");
	            		try (ResultSet rs = st.executeQuery()) {
	                        System.out.println("Pelicules trobades per nom: '"+botiga+"':");
	                        if (!rs.next()){
	                        	System.out.println("NOT FOUND");
	                        }
	                        while (rs.next()) {
	                            String title=rs.getString("f.title");
	                            String retorn=rs.getString("retorn");
	                            System.out.println(title+"\t NO DISPONIBLE, Data de retorn: "+retorn+"\t");
	                        }
	                    }
	    			
	        } catch (SQLException e) {
	            System.err.println("Error SQL: " + e.getMessage());
	        }
	        
		}
		
		public static void cercaActor(int botiga, String cerca){
			
			String url = "jdbc:mysql://localhost:3306/sakila";
			Properties connectionProperties = new Properties();
	        connectionProperties.setProperty("user", "root");
	        try (Connection con = DriverManager.getConnection(url,connectionProperties);
	            PreparedStatement st = (PreparedStatement) con.prepareStatement("SELECT f.title"
	            		+ " FROM film f JOIN inventory i ON f.film_id = i.film_id"
	            		+ " JOIN store s ON i.store_id = s.store_id"
	            		+ " WHERE s.store_id = ? AND upper(f.title) LIKE ?")) {          
	            		st.setInt(1, botiga);
	            		st.setString(2, cerca);
	            		try (ResultSet rs = st.executeQuery()) {
	                        System.out.println("Pelicules trobades per actor:");
	                        if (!rs.next()){
	                        	System.out.println("NOT FOUND");
	                        }
	                        while (rs.next()) {
	                            String title=rs.getString("f.title");
	                            System.out.println(title+"\t");
	                        }
	                    }
	    			
	        } catch (SQLException e) {
	            System.err.println("Error SQL: " + e.getMessage());
	        }
	        
		}
		
		public static void cercaGenere(int botiga, String cerca){
	
			String url = "jdbc:mysql://localhost:3306/sakila";
			Properties connectionProperties = new Properties();
	        connectionProperties.setProperty("user", "root");
	        try (Connection con = DriverManager.getConnection(url,connectionProperties);
	            PreparedStatement st = (PreparedStatement) con.prepareStatement("SELECT f.title"
	            		+ " FROM film f JOIN inventory i ON f.film_id = i.film_id"
	            		+ " JOIN store s ON i.store_id = s.store_id"
	            		+ " WHERE s.store_id = ? AND upper(f.title) LIKE ?")) {         
	        	
	            		st.setInt(1, botiga);
	            		st.setString(2, cerca);
	            		
	            		try (ResultSet rs = st.executeQuery()) {
	                        System.out.println("Pelicules trobades per genere:");
	                        if (!rs.next()){
	                        	System.out.println("NOT FOUND");
	                        }
	                        while (rs.next()) {
	                            String title=rs.getString("f.title");
	                            System.out.println(title+"\t");
	                        }
	                    }
	    			
	        } catch (SQLException e) {
	            System.err.println("Error SQL: " + e.getMessage());
	        }
	  
		}
		
	}